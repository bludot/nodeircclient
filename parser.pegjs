start
  = message

message
  = ":" prefix: prefix "\x20" command: $ command ? params:( params ) ? CRLF { return {prefix:prefix, command: command, params: params }}
  / command: $ command ? params:( params ) ? CRLF { return {command: command, params: params }}
  / CRLF

prefix
  = nickname: $ nickname "!" user: $ user "@" host: $ host {return {nickname: nickname, user: user, host: host} }
  / servername: $ servername { return { servername: servername }}

command
  = letter+ / digit+

params
  =  ($ ("\x20" middle) { return text().slice(1) })+ ( ("\x20" ":" trailing ) { return text().slice(2) }) ?
  /   ($ ("\x20" middle) { return text().slice(1) })+ ( ("\x20" ":"? trailing) { return text().slice(2) }) ?
  /   ($ ("\x20" ":"? middle) { return text().substr(0,2) === " :" ? text().slice(2) : text().slice(1) })+ ( ("\x20" ":" trailing) { return text().substr(0,1) === ":" ? text().slice(3) : text().slice(2) }) ?

middle
  = nospcrlfcl (":" / nospcrlfcl)*

trailing
  = (":" / "\x20" / nospcrlfcl)*

nospcrlfcl
 = [\x01-\x09] / [\x0B-\x0C] / [\x0E-\x1F] / [\x21-\x39] / [\x3B-\xFF]

servername
  = hostname

host
  = hostname / hostaddr

hostname
  = shortname ("."  shortname)*

shortname
  = irctextcode? (letter / digit ) (letter / digit / "-" / "/" / "." / irctextcode)*

irctextcode
  = ( [\x00-\x09] / ("\x02" / "\x03" / "\x1D" / "\x1F" / "\x16" / "\x0F"))+

user
  = ( special ) ? nickname

hostaddr
  = IPv4address / IPv6address

HEXDIG
  = DIGIT
  / "A"i
  / "B"i
  / "C"i
  / "D"i
  / "E"i
  / "F"i

DIGIT
  = [\x30-\x39]
dec_octet
  = $( "25" [\x30-\x35]      // 250-255
     / "2" [\x30-\x34] DIGIT // 200-249
     / "1" DIGIT DIGIT       // 100-199
     / [\x31-\x39] DIGIT     // 10-99
     / DIGIT                 // 0-9
     )
IPv4address
  = $(dec_octet "." dec_octet "." dec_octet "." dec_octet)
IPv6address
  = $(                                                            h16_ h16_ h16_ h16_ h16_ h16_ ls32
     /                                                       "::"      h16_ h16_ h16_ h16_ h16_ ls32
     / (                                               h16)? "::"           h16_ h16_ h16_ h16_ ls32
     / (                               h16_?           h16)? "::"                h16_ h16_ h16_ ls32
     / (                         (h16_ h16_?)?         h16)? "::"                     h16_ h16_ ls32
     / (                   (h16_ (h16_ h16_?)?)?       h16)? "::"                          h16_ ls32
     / (             (h16_ (h16_ (h16_ h16_?)?)?)?     h16)? "::"                               ls32
     / (       (h16_ (h16_ (h16_ (h16_ h16_?)?)?)?)?   h16)? "::"                               h16
     / ( (h16_ (h16_ (h16_ (h16_ (h16_ h16_?)?)?)?)?)? h16)? "::"
     )

ls32
  // least_significant 32 bits of address
  = h16 ":" h16
  / IPv4address

h16_
  = h16 ":"

h16
  // 16 bits of address represented in hexadecimal
  = $(HEXDIG (HEXDIG (HEXDIG HEXDIG?)?)?)

nickname
= ( special / letter ) (letter / digit / special)+

special
  = '~' / '-' / '_' / '|' / '[' / ']' / '{' / '}'

CRLF
  = "\x0D" "\x0A"

digit
  = [0-9]

letter
  = [a-z] / [A-Z]
